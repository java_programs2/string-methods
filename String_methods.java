package string_basics;

import java.util.Arrays;

public class String_methods {

	public static void main(String[] args) {
	String s="praveenkumar";
	String s1="BE";
//1.length();
		System.out.println(s.length());
//2.charAt();
		
		System.out.println(s.charAt(0));
	
//3.substring		
		System.out.println(s.substring(3));
		System.out.println(s.substring(3, 6));
		
//4.concat
		System.out.println(s.concat(s1));
		
//5indexOf
		String s2="hello praveen welcome";
		String s3="this index is";
		System.out.println(s2.indexOf("welcome")); // for string
		System.out.println(s3.indexOf("is", 4));
		System.out.println(s2.indexOf('p'));  //for char
//6 lastIndexOf
		System.out.println(s.lastIndexOf("a"));  // for string
		System.out.println(s.lastIndexOf('a'));  // for char
		
// 7 equals
		String a="geeks";
		String b="geeks";
		System.out.println(a.equals(b));
// 8 eqalsIgnoreCase();
		String c="GEEKS";
		System.out.println(a.equalsIgnoreCase(c));
		
//9 compareTo -lexicograghy by using alphabet order it return 0, -ve values and +positive values
		
		String w="kite";
		String q="hello";
		System.out.println(w.compareTo(q));  // k is 11 & h is 8 so 11-8=3

//10 compareToIgnoreCase
		
		String w1="Kite";
		String q1="hello";
		System.out.println(w1.compareToIgnoreCase(q1));  // k is 11 & h is 8 so 11-8=3

//11 toLowerCase()
String q2= "PRAVEEN";
System.out.println(q2.toLowerCase());

//12 toUpperCase()
String q3= "praveen";
System.out.println(q3.toUpperCase());
// 13 trim()

String s5=" praveen kumar ";
System.out.println(s5.trim());

//14 replace() - replace old char to new char in a string
String s6="java";
System.out.println(s6.replace('a', 'd'));

// 15 contains() - returns true if the string contains the given string

String d1="hello welcome";
String d2="hello";
System.out.println(d1.contains(d2));

	
//16 toCharArray();
String d3="praveen";
char ch[]=d3.toCharArray();
System.out.println(Arrays.toString(ch));

// 17 startsWith() - statts with specific string

String d4="kitkat";
System.out.println(d4.startsWith("kit"));  // it returns true

//18. ends with
	String c1="praveenkumar";
	String c2="kumar";
	
	System.out.println(c1.endsWith(c2));
	
	
	// 19 isEmpty
	System.out.println(c1.isEmpty());
	
	//20. trim()
	String a1=" hello ";
	System.out.println(c1.trim());
	
	//21 split
	String q4="hello praveen kumar";
	System.out.println(Arrays.toString(q4.split(" ")));

}
	
	
}
